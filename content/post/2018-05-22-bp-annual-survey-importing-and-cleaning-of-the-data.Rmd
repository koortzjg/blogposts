---
title: BP Annual Survey - Importing and Cleaning of the Data
author: Hardi Koortzen
date: '2018-05-22'
categories:
  - R
tags:
  - BP Annual Survey
  - Quandl
  - R
  - rvest
  - tidyverse
slug: bp-annual-survey-importing-and-cleaning-of-the-data
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(
  echo = TRUE,
  message = FALSE,
  warning = FALSE,
  fig.align = "center",
  fig.asp = 0.618,
  fig.width = 6,
  out.width = "95%"
)

library("extrafont")
library("rforworkr")
library("ggrepel")

ggplot2::theme_set(theme_blog())
```

Working in the Petrochemical industry, I am interested in oil and gas reserves in the world. It is difficult to find historical data for oil and gas reserves. [BP](https://on.bp.com/2rQZR61) has an annual survey, dating back to the 1980's, and this is updated yearly. The file one can download, in `Excel` format, is not the best to work with and requires allot of wrangling to get it into a decent format. Fortunately, this data is also available via [Quandl](http://bit.ly/quandl_BP). This lags a bit on the reported data, however, this is easier to work with, as it is a call to their API. There is no need to register, one can simply just download the package and interact with their API, although this is limited to 50 calls a day, and a limit of 20 calls per 10 minutes. Registration is free, and allows one to have more calls (a limit of 300 calls per 10 seconds, 2,000 calls per 10 minutes and a limit of 50,000 calls per day) to the site. Registration will give one an API key, which can then be used during the calls in the `Quandl` functions. In order to freely distribute code, I have followed the guidance of [others](http://bit.ly/2k6r1SX), and created a `.Renviron` file, which contains my Quandl API key. Just one line, and that is all. We will later see how to use it.

```
quandl_key = your_api_key_here
```

This is a big dataset, and contains various codes to be downloaded. Now one can simply find the codes, by searching for the codes, create a `tibble()` and `map()` the data, however, this is very time consuming, and one needs to redo this frequently, since codes might be added. I am not in the habit of making my life more difficult, thus I needed to find an alternative.

Fortunately, one can download a `.zip` file, containing all the codes, again, not one for doing this manually, I needed a way of doing it reproducibly in `R`. Some searching let me to this Stack Overflow [question](http://bit.ly/2Iv3OZ2). Now, I had some issues with the file I wanted to download, and could not get my code replacement to work. Well, a bit of preservation, let me to the following.

```{r}
library("tidyverse")

bp_codes_file <- tempfile()

"https://www.quandl.com/api/v3/databases/BP/codes" %>%
  paste(., "?api_key=", Sys.getenv("quandl_key"), sep = "") %>%
  download.file(
    url      = .,
    destfile = bp_codes_file,
    mode     = "wb",
    quiet    = TRUE
  )

unzip(zipfile = bp_codes_file, exdir = tempdir()) %>%
  read_csv(., col_names = FALSE) %>%
  select(., code = X1, description = X2) %>%
  arrange(., description) ->
  bp_codes
```

```{r, echo = FALSE}
bp_codes %>%
  glimpse(.)
```

A few things here, I really wanted to 'pipe' this, however, I had issues with the `unzip()` function in the pipe, thus the code here. We start by first creating a temporary file (`tempfile()`), next we download the file to this temporary file, using `download.file()`. If you have registered, and saved the API key to your `.Renviron` file, we can `paste()` these together, and not be worried about sharing our API key. The `.csv` file, does not have header rows, and the argument `col_names = FALSE` needs to be used. I used the `select()` function, to change the column names, and to select only the columns we are interested in, although this is only two.

Now, I mentioned that this is a big dataset, and here we have `r nrow(bp_codes)` observations. Now that is quite a bit of codes, and thus the manual, search and create, would be quite long and time consuming. Now we could probably use all these and get all the data, however, this will be really big, and also would require further filtering and wrangling, before using these.

Since these are subdivided into section, not as obvious yet, we can rather just select the codes we are interested in for a particular section. OK, I confused myself there, so let me give an example. I am only interested in the 'Proved Oil Reserves' for all the countries. This can be used to filter the codes to those for that only, and then we can download these.

```{r}
bp_codes %>%
  filter(., str_detect(description, "Proved Oil Reserves")) ->
  proved_oil_reserves
```

```{r, echo = FALSE}
proved_oil_reserves %>%
  glimpse(.)
```

This is better. This is reduced to only `r nrow(proved_oil_reserves)` codes now. Upon closer inspection, there are ones which are an aggregation of areas, i.e 'Total Africa', and these can be removed, since we are do not require those.

```{r}
proved_oil_reserves %<>%
  filter(., !str_detect(description, "Total"))
```

```{r, echo = FALSE}
proved_oil_reserves %>%
  glimpse(.)
```

Since we will be using the data to look at maps, it will be worthwhile to get the names of the countries and regions. First we can extract the country names from the `description` column. While we are working with the names, we can start by getting them to a state where they will be compatible with the `map_data("world")` dataset. This is not obvious now, and can be done later, if one so desires. A new column (`country`) was also created, and this just extract the names from the `decription` column, and will be use to plot and aggregate the data.

```{r}
proved_oil_reserves %<>%
  mutate(., description = str_replace(description, "Trinidad & Tobago", "Trinidad")) %>%
  mutate(., description = str_replace(description, "United Kingdom$", "UK")) %>%
  mutate(., country = if_else(
    !str_detect(description, "Other"),
    str_remove(description, "Proved Oil Reserves - "),
    NA_character_
  ))
```

```{r, echo = FALSE}
proved_oil_reserves %>%
  glimpse(.)
```

Next is to find the region where these countries are, i.e. the continents, however, I want to keep the 'Middle East' separate, since this region is an important one in the oil industry. Again, we can all do this with nested `if_else()` functions, but to find all the countries and regions will just be too time consuming. Hopefully there are already tables on the web that can help us? Luckily, Wikipedia already give us these information, see [here](http://bit.ly/sovereign_states_and_dependent_territories_by_continent) and [here](http://bit.ly/middle_east_countries). We can get this table using the `rvest` package, and with a bit of name cleaning, all the countries will have a region. Since there are 7 tables to get the data from, and adhering, somewhat, to the only copy and paste three times, before writing a function, I wrote a function (`get_countries_region()`).

```{r}
get_countries_region <- function(url, x_path, col_name) {
  # function for getting the countries and regions from the Wikipedia site
  # params:
  #         url link to the page you want to read
  #         x_path is the XPath to the required table
  #         col_name is the column name used to get the country name
  # return: a tibble containing the countries in that region
  
  url %>%
    read_html(.) %>%
    html_node(., xpath = x_path) %>%
    html_table(., fill = TRUE) %>%
    as_tibble(.) %>%
    select(., country = starts_with(col_name)) %>%
    # removed strings between brackets [()]
    mutate(., country = str_remove(country, "\\((.*?)\\)")) %>%
    # ensuring that no whitespaces are present in the country name
    mutate(., country = str_trim(country)) %>%
    mutate(.,
           country = str_replace(
             country,
             "Democratic Republic of the Congo",
             "Republic of Congo")) %>%
    mutate(., country = str_replace(country, "Russia$", "Russian Federation")) %>%
    mutate(., country = str_replace(country, "Trinidad and Tobago", "Trinidad")) %>%
    mutate(., country = str_replace(country, "United Kingdom$", "UK")) %>%
    mutate(., country = str_replace(country, "United States$", "USA")) %>%
    return(.)
  
}
```

So what does this function do? It will look at the `url`, and extract the table (`x_path`) specified. It then select the `.col_name` specified, and rename it to `country`. Then removes all text between and including the brackets `"\\((.*?)\\)"`. Next, it will change a few country names, this is done to make them compatible with the `map_data("world")` dataset. Now this function can be used to get the countries in the specified regions.

```{r}
library("rvest")

tribble(
  ~ x_path,                                   ~ region,
  '//*[@id="mw-content-text"]/div/table[2]',  "Africa",
  '//*[@id="mw-content-text"]/div/table[4]',  "Asia Pacific",
  '//*[@id="mw-content-text"]/div/table[6]',  "Europe & Eurasia",
  '//*[@id="mw-content-text"]/div/table[8]',  "North America",
  '//*[@id="mw-content-text"]/div/table[10]', "S. & Cent. America",
  '//*[@id="mw-content-text"]/div/table[12]', "Oceania",
  '//*[@id="mw-content-text"]/div/table[13]', "Antarctica"
  ) %>%
  mutate(
    .,
    data = map(
      .x       = x_path,
      .f       = get_countries_region,
      url      = "http://bit.ly/sovereign_states_and_dependent_territories_by_continent",
      col_name = "Name"
    )
  ) %>%
  unnest(., data) %>%
  select(., country, region) %T>%
  # saving file for later
  write_rds(., here::here("static/data/world_countries.rds"), "xz") %>%
  left_join(proved_oil_reserves, ., by = "country") %>%
  mutate(., region = if_else(str_detect(country, "USSR"), "Europe & Eurasia", region)) %>%
  mutate(., region = if_else(
    is.na(region),
    str_remove(description, "Proved Oil Reserves - Other "),
    region
  )) %>%
  arrange(., region) ->
  proved_oil_reserves
```

```{r, echo = FALSE}
proved_oil_reserves %>%
  glimpse(.)
```

Now we can use this custom function to get the tables on the Wikipedia pages. First, we create a `tibble()`, containing the XPath (`x_path`) and the `region`. Next we `map()` this to the column `data`, specifying the `url` and the `col_name`. Now, one can create a `tibble()` containing those as well, and then using `pmap()` to map all these, however, since the `url` and `col_name` are the same for all, I decided to just assign these during the `map()` call. Next we `unnest()` and select the country and region columns. Now we can join this to our original data. Here `left_join()` works best, with the original data being the `x` data, and the country data the `y`. Why do it this way? Well, we only want to get the region for those countries in our dataset. This eliminate some additional filtering later.

Now this is all good and well, however, I am still missing the 'Middle East' region. To find the countries in that region, requires another link. Now at least we have a function that will help us to get these.

```{r}
get_countries_region(url = "http://bit.ly/middle_east_countries",
                     x_path = '//*[@id="mw-content-text"]/div/table[2]',
                     col_name = "Country") %>%
  # to distinguish it from the original region
  # will be removed later
  mutate(., region1 = "Middle East") %T>%
  # saving file for later
  write_rds(., here::here("static/data/middle_east_countries.rds"), "xz") %>%
  left_join(proved_oil_reserves, ., by = "country") %>%
  mutate(., region = if_else(is.na(region1), region, region1)) %>%
  select(., -region1) %>%
  arrange(., region) ->
  proved_oil_reserves
```

```{r, echo = FALSE}
proved_oil_reserves %>%
  glimpse(.)
```

Now that we have all the codes, we can get the data for the proved oil reserves for all the countries in the BP dataset. This was quite a lot of code to just get the countries and regions from the codes and description of the original data, however, it will pay-off quite soon. At least now we have columns for the countries and the region, and can just focus on getting the data. This can be done using the `Quandl` package for the Quandl API. Registering really pays off here, as we will be doing quite a few calls in a short period of time. Here we start with our cleaned codes, and `map()` this to the `Quandl()` function. We keep the defaults, so no need for additional arguments. This should now be familiar, as we are recycling things here. I am doing it this way, thus keeping all our hard work in getting the country and regions together with the data.

```{r, eval = FALSE}
library("Quandl")

Quandl.api_key(Sys.getenv("quandl_key"))

proved_oil_reserves %<>%
  mutate(., data = map(code, Quandl)) %>%
  unnest(., data) %>%
  select(.,
         code:region,
         date = Date,
         oil_reserves_billion_barrels = Value) %>%
  arrange(., country, desc(date)) %>%
  mutate(., region = fct_relevel(region, "Oceania", after = Inf)) %T>%
  # save data to limit calls to API
  write_rds(., here::here("static/data/proved_oil_reserves.rds"), "xz")
```

```{r, echo = FALSE}
proved_oil_reserves <- read_rds(here::here("static/data/proved_oil_reserves.rds"))

proved_oil_reserves %>%
  glimpse(.)
```

And there it is. The proved oil reserves, per country and region, since 1980. An additional column here is the `oil_reserves_billion_barrels`, and that is just to give the column a name that tells one what it contains, and in what units it is in. If we so desire, we can change the units to something else, but we do not have to consult the API to understand what units we have now. Our data is also in a good place and we can start to visualise it.

```{r plt-oil-reserves-world, echo = FALSE, fig.cap = "Oil reserves of the world."}
proved_oil_reserves %>%
  filter(., !is.na(country), country != "USSR") %>%
  group_by(., country) %>%
  summarise(.,
            oil_reserves_billion_barrels = max(oil_reserves_billion_barrels)) %>%
  mutate(., country = str_replace(country, "Russian Federation", "Russia")) %>%
  left_join(., map_data("world"), by = c("country" = "region")) %>%
  ggplot(., aes(x = long, y = lat, group = group)) +
  geom_map(
    data    = map_data("world"),
    fill    = "grey",
    map     = map_data("world"),
    mapping = aes(map_id = region)
  ) +
  geom_map(
    map         = map_data("world"),
    mapping     = aes(
      fill      = log(oil_reserves_billion_barrels),
      map_id    = country
    ),
    show.legend = FALSE
  ) +
  geom_polygon(colour = "white",
               data = map_data("world"),
               fill = NA) +
  viridis::scale_fill_viridis(option = "plasma") +
  labs(title    = "Proved Oil Reserves per Country",
       subtitle = "Brighter colours represent higher reserves") +
  coord_map(
    xlim = c(-180, 180),
    ylim = c(-90, 90),
    projection = "mollweide"
  ) +
  theme_void() +
  theme(panel.background = element_rect(fill = "light blue"))
```

Figure \@ref(fig:plt-oil-reserves-world) shows where the countries are that are currently covered in the BP results, and the colour indicates the amount of reserves, i.e. brighter colours represent more reserves. Note the bright colours, which means that some countries are really fortunate, and have access to vast amounts of resources, while others, do not have these, and are reliant on these oil producing countries.

```{r plt-total-region, echo = FALSE, fig.cap = "Proved oil reserves per region, since 1980."}
set.seed(42)

proved_oil_reserves %>%
  filter(., date == max(date)) %>%
  group_by(., date, region) %>%
  summarise(.,
            oil_reserves_billion_barrels = sum(oil_reserves_billion_barrels)) %>%
  arrange(., desc(region)) %>%
  ungroup(.) %>%
  mutate(., new_col = lead(oil_reserves_billion_barrels) / 2) %>%
  mutate(., new_col = lag(new_col)) %>%
  mutate(., new_col2 = cumsum(oil_reserves_billion_barrels)) %>%
  mutate(., new_col2 = lag(new_col2) + new_col) %>%
  mutate(
    .,
    new_col2 = if_else(is.na(new_col2), oil_reserves_billion_barrels, new_col2),
    date = lubridate::year(date)
  ) %>%
  select(., date, region, oil_reserves_billion_barrels = new_col2) ->
  plt_labels

proved_oil_reserves %>%
  mutate(., date = lubridate::year(date)) %>%
  ggplot(., aes(x = date, y = oil_reserves_billion_barrels)) +
  geom_col(aes(fill = region), show.legend = FALSE, width = 0.8) +
  scale_fill_blog() +
  scale_colour_blog() +
  coord_cartesian(xlim = c(1980, 2030),
                  ylim = c(0, 2000)) +
  labs(
    title   = "Proved Oil Reserves per Country",
    caption = "data from https://www.quandl.com/ and https://www.bp.com",
    x       = "Year",
    y       = "Billion Barrels"
  )  +
  geom_text_repel(
    data        = plt_labels,
    family      = "Cambria",
    force       = 1,
    label.size  = NA,
    mapping     = aes(colour = region, label = region),
    nudge_x     = 10,
    show.legend = FALSE,
    size        = 3
  )
```

Figure \@ref(fig:plt-total-region) indicate the growth in the total proved oil reserves since 1980. Some regions, i.e. Oceania, have a low amount of reserves, and it is difficult to see on the graph. It is interesting to see the 'jumps' in the data, suggesting that at some point more oil reserves have become accessible. This could be due to better exploration, or getting more from the sources, as the experience goes up and overall costs comes down. Now, it is clear that there are a finite amount of these fossil fuels, and that at some stage we will be depleting these reserves. There are been a levelling off in the last few years, indicating that the oil being extracted and the oil being found are about the same. It will be interesting to observe this trend over the next few years, to see how responsible we are in utilising these resources.

There we have a good overview of the proved world oil reserves. The next steps can include a comparison between the proved oil reserves and the proved gas reserves ('Proved Natural Gas Reserves'), and this will be repeat of the above, only substituting the filtering for 'Proved Natural Gas Reserves', since this will be the codes for the gas reserves.
