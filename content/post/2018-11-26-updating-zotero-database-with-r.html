---
title: Updating Zotero Database With R
author: Hardi Koortzen
date: '2018-11-26'
categories:
  - R
tags:
  - R
  - Zotero
slug: updating-zotero-database-with-r
---



<p>During my career as a researcher, I have collected loads of references, ranging from articles to reports. As with all things in my life, the software used for keeping track of this has changed. Currently I am using <a href="https://www.zotero.org/">Zotero</a>. I am striving to use mostly open source software, thus the move to this. Recently, I started to add lots of PDFs to this database. There were quite a few thousand files, and took a significant amount of time, probably also due to the limited RAM in my Windows machine. Zotero gives you duplicates, and after cleaning this, one can slit find some of the fields that are not correct. The ones I am focussed on here is the journal name and journal abbreviations. These are excellent in visualising the data. During the import, even using the web importing facility, these fields might not be correct. During the data analysis, I manage to update these fields using the power of <code>R</code>. Now, this is not as trivial as it first seems, and thus this post.</p>
<p>Zotero uses an <code>.sqlite</code> database file. There are quite a few tables in this database, containing all the required information for maintaining all the references. This was/is really still daunting, and requires that one need to go slow with these. I usually make a copy, and work on the copy, until I am ready to move to the real database. Four tables are or importance here: <code>items</code>, <code>itemData</code>, <code>itemDataValues</code>, and <code>fields</code>. The <code>fields</code> table is the smallest, and is used to get the <code>fieldID</code> from the <code>fieldName</code>. We need these to correctly insert or update certain field values. Now, there is no way that I can make my Zotero database available to all, thus the code will only work for me. <strong>You can of course connect to your own Zotero database, and use the code carefully on a copy. I do not take responsibility for code not working, or damaging the database. Always work with a copy of the database.</strong></p>
<p>When I first opened the Zotero <code>.sqlite</code> database, it did not make sense, and I must admit, I am still struggling. I was not used to the way the developers used the database. I am more familiar with a more table oriented view, and kind of expected this database to be the same. I am still learning all the ins and outs of this, but it seems like an interesting and more scalable way of doing things. One can always <em>bind</em> all the tables in the final visualising with a good <code>SQL</code> query. OK, so back to my issue. I wanted to change all the <code>publicationTitle</code> and <code>journalAbbreviation</code> fields to reflect the correct journal and abbreviation. I did manage to find a <code>.txt</code> file containing several thousand journals and their associated abbreviations. This is also still a work in progress, since I do not use all the journals and some have the same abbreviation, which violates some of the joining rules. Unfortunately, I do not remember where I downloaded this from, and thus cannot direct the reader to this, apologies. Just another of the reasons I require a reference manager.</p>
<p>Zotero gives each value a unique <code>valueID</code> number, and it then uses this number to bind it to the correct field for that item. Thus, in order to update or insert a value in an item, it needs to have a <code>valueID</code>. This is not a problem for the correct journal and abbreviation that are already in the database, however, it gets a bit tricky if this is not in the database. Here I am still struggling a bit, and might write a follow up when this is solved. Fortunately, for the majority of the journals, both the correct journal and abbreviation are already in this table. All that is required is to ensure that the item also contains the correct values.</p>
<p>The following packages are required for using all the code here, as well as to establish a connection to the database:</p>
<pre class="r"><code>library(purrr)
library(readr)
library(stringr)
library(tidyr)
library(magrittr)
library(dplyr)

conn_zot &lt;- DBI::dbConnect(RSQLite::SQLite(), dbname = &quot;zotero.sqlite&quot;)</code></pre>
<p>First we need to find these values. This can be done with the following <code>SQL</code> query:</p>
<pre class="sql"><code>SELECT *
  FROM itemDataValues</code></pre>
<p>This dataset contains all the data, however, this is not the best way of looking at one’s data. The reason for getting this will become clear later. This can be quite a big dataset, depending on the database. Now, let us get a dataset containing all the journals, their journal names and abbreviations.</p>
<pre class="sql"><code>SELECT i.itemID AS itemID,
       pub.value AS publicationTitle,
       journalAbbreviation.value AS journalAbbreviation,
       i.key AS zotero_key
  FROM items i
INNER JOIN itemDataValues pub
ON pub.valueID = (SELECT itemData.valueID
                    FROM itemData
                   WHERE itemData.fieldID = (SELECT fieldID
                                               FROM fields
                                              WHERE fields.fieldName = &#39;publicationTitle&#39;
                                              LIMIT 1)
                         AND itemData.itemID = i.itemID
                   LIMIT 1)
LEFT JOIN itemDataValues journalAbbreviation
ON journalAbbreviation.valueID = (SELECT itemData.valueID
                                    FROM itemData
                                   WHERE itemData.fieldID = (SELECT fieldID
                                                               FROM fields
                                                              WHERE fields.fieldName = &#39;journalAbbreviation&#39;
                                                              LIMIT 1)
                                         AND itemData.itemID = i.itemID
                                   LIMIT 1)
LEFT JOIN deletedItems
ON i.itemID = deletedItems.itemID
 WHERE deletedItems.itemID IS NULL</code></pre>
<p>A few things before we move on, there are quite a few nested queries here, and this is needed to bind the journal name (<code>publicationTitle</code>) and the journal abbreviation (<code>journalAbbreviation</code>) fields to the correct items. Now, since this is my personal database, I could have used the <code>fieldID</code> value for the <code>publicationTitle</code> and <code>journalAbbreviation</code> found in the <code>fields</code> table, however, I did not want to do this, since this requires me to look up that value for each of my Zotero databases (not that I have more than one). This also ensures that this code will work on any Zotero database.</p>
<p>In order to update the database, a <code>SQL</code> query is needed. This is where one can break the database, so ensure that all the testing is done on a copy before moving onto the real database. It is never good to loose the original database where one have put in years’ worth of data. This is a ‘simple’ function, that takes two arguments, <code>valueID</code> and <code>itemID</code>, and then it <code>UPDATE</code> the <code>SQL</code> database with a new <code>publicationTitle</code>. For this I have used the <code>glue</code> package, since it allows for easy insertion of fields.</p>
<pre class="r"><code>update_sql &lt;- function(valueID, itemID) {
  glue::glue_sql(&quot;UPDATE itemData
                     SET valueID = {valueID}
                   WHERE itemID = {itemID}
                         AND itemData.fieldID = (SELECT fieldID
                                                   FROM fields
                                                  WHERE fields.fieldName = &#39;publicationTitle&#39;
                                                  LIMIT 1)&quot;, .con = conn_zot) %&gt;%
    DBI::dbGetQuery(conn = conn_zot, statement = .)
}</code></pre>
<p>This is the first step, and up to now, we have just read from the database. The next stage is to find the values of the current title, compare it with the correct title, and insert the correct title.</p>
<pre class="r"><code>data_zotero %&gt;%
  as_tibble() %&gt;%
  mutate(tolower = str_to_lower(publicationTitle)) %&gt;%
  mutate(tolower = str_remove_all(tolower, &quot;[:punct:]&quot;)) %&gt;%
  mutate(tolower = str_squish(tolower)) %&gt;%
  inner_join({
    &quot;journal_abbreviations.txt&quot; %&gt;%
      read_lines() %&gt;%
      as_tibble() %&gt;%
      select(journal = value) %&gt;%
      separate(
        col = journal,
        into = c(&quot;journal&quot;, &quot;abbreviation&quot;),
        sep = &quot; = &quot;
      ) %&gt;%
      mutate(tolower = str_to_lower(journal)) %&gt;%
      mutate(tolower = str_remove_all(tolower, &quot;[:punct:]&quot;)) %&gt;%
      mutate(tolower = str_squish(tolower)) %&gt;%
      identity()
  }, by = &quot;tolower&quot;) %&gt;%
  inner_join(zot_item_values, by = c(&quot;journal&quot; = &quot;value&quot;)) %&gt;%
  filter(publicationTitle != journal) %&gt;%
  identity() %$%
  walk2(valueID, itemID, update_sql)</code></pre>
<p>OK, so let’s break this down a bit. First I change all the titles to lower case, and remove all punctuations and white spaces. This is to ensure that most of the versions are normalised. We require three <code>mutate()</code> calls, since we are changing the same variable several times. Next we need need to find the ‘correct’ values from the <code>.txt</code>, thus the <code>inner_join()</code>. Since we have a column having the same name (<code>tolower</code>), we just join these two on this. In the <code>.txt</code> file, one therefore needs the correct version of both the title and the abbreviation, and this needs to only occur once in this file (in the one I downloaded, this was not always the case). Now the next step is to ensure that the title is already in the <code>SQL</code> database. As mentioned earlier, I am doing this by hand for every new journal (just once), but after that, this will work fine. Next we filter the ones where the <code>publicationTitle</code> is not equal to the <code>journal</code> variable. These are the ones we need to update the title. The final step is to use the <code>walk2()</code> function to update all these occurrences with the new journal title, with the function we have created earlier. And that is the basis for updating.</p>
<p>Now sometimes the <code>publicationTitle</code> contains the actual abbreviation of the journals, since importing the data into Zotero does rely on some meta-data in the PDF or website. A similar approach is followed for this.</p>
<pre class="r"><code>data_zotero %&gt;%
  as_tibble() %&gt;%
  mutate(tolower = str_to_lower(publicationTitle)) %&gt;%
  mutate(tolower = str_remove_all(tolower, &quot;[:punct:]&quot;)) %&gt;%
  mutate(tolower = str_squish(tolower)) %&gt;%
  inner_join({
    &quot;journal_abbreviations.txt&quot; %&gt;%
      read_lines() %&gt;%
      as_tibble() %&gt;%
      select(journal = value) %&gt;%
      separate(
        col = journal,
        into = c(&quot;journal&quot;, &quot;abbreviation&quot;),
        sep = &quot; = &quot;
      ) %&gt;%
      mutate(tolower = str_to_lower(abbreviation)) %&gt;%
      mutate(tolower = str_remove_all(tolower, &quot;[:punct:]&quot;)) %&gt;%
      mutate(tolower = str_squish(tolower)) %&gt;%
      identity()
  }, by = &quot;tolower&quot;) %&gt;%
  inner_join(zot_item_values, by = c(&quot;journal&quot; = &quot;value&quot;)) %&gt;%
  filter(publicationTitle != journal) %&gt;%
  identity() %$%
  walk2(valueID, itemID, update_sql)</code></pre>
<p>No need to walk through the code here. It is similar to the previous one, just a difference in the <code>journal_abbreviations</code> data, where the <code>abbreviation</code> is used.</p>
<p>Now for the journal abbreviations fields, a new query needs to be created, since we want to update a different field.</p>
<pre class="r"><code>update_sql &lt;- function(valueID, itemID) {
  glue::glue_sql(&quot;UPDATE itemData
                     SET valueID = {valueID}
                   WHERE itemID = {itemID}
                         AND itemData.fieldID = (SELECT fieldID
                                                   FROM fields
                                                  WHERE fields.fieldName = &#39;journalAbbreviation&#39;
                                                  LIMIT 1)&quot;, .con = conn_zot) %&gt;%
    DBI::dbGetQuery(conn = conn_zot, statement = .)
}</code></pre>
<p>The rest is pretty much the same as the previous two. I did try and create a function, however, I am struggling with all the <code>enquo()</code>’s and <code>!!</code> notation of the <code>rlang</code> package. I especially find the last <code>inner_join()</code> troublesome, thus the copy and pasting.</p>
<pre class="r"><code>data_zotero %&gt;%
  as_tibble() %&gt;%
  mutate(tolower = str_to_lower(publicationTitle)) %&gt;%
  mutate(tolower = str_remove_all(tolower, &quot;[:punct:]&quot;)) %&gt;%
  mutate(tolower = str_squish(tolower)) %&gt;%
  inner_join({
    &quot;journal_abbreviations.txt&quot; %&gt;%
      read_lines() %&gt;%
      as_tibble() %&gt;%
      select(journal = value) %&gt;%
      separate(
        col = journal,
        into = c(&quot;journal&quot;, &quot;abbreviation&quot;),
        sep = &quot; = &quot;
      ) %&gt;%
      mutate(tolower = str_to_lower(journal)) %&gt;%
      mutate(tolower = str_remove_all(tolower, &quot;[:punct:]&quot;)) %&gt;%
      mutate(tolower = str_squish(tolower)) %&gt;%
      identity()
  }, by = &quot;tolower&quot;) %&gt;%
  inner_join(zot_item_values, by = c(&quot;abbreviation&quot; = &quot;value&quot;)) %&gt;%
  filter(journalAbbreviation != abbreviation) %&gt;%
  identity() %$%
  walk2(valueID, itemID, update_sql)</code></pre>
<p>The final step, is to insert the journal abbreviations into the <code>SQL</code> database for those journals that does not have any abbreviations. Note that now we have change the query from and <code>UPDATE</code> query to an <code>INSERT</code> one.</p>
<pre class="r"><code>update_sql &lt;- function(valueID, itemID) {
  glue::glue_sql(&quot;INSERT INTO itemData (itemID, fieldID, valueID) 
                  SELECT {itemID},
                         (SELECT fieldID
                            FROM fields
                           WHERE fields.fieldName = &#39;journalAbbreviation&#39;
                           LIMIT 1),
                         {valueID}&quot;, .con = conn_zot) %&gt;%
    DBI::dbGetQuery(conn = conn_zot, statement = .)
}</code></pre>
<p>The rational here is similar. First we need to find all the items that does not have an abbreviation, and then insert it with the corresponding value. Depending on the size of the database, this can take quite awhile. Fortunately, it goes faster after the first iteration.</p>
<pre class="r"><code>data_zotero %&gt;%
  as_tibble() %&gt;%
  mutate(tolower = str_to_lower(publicationTitle)) %&gt;%
  mutate(tolower = str_remove_all(tolower, &quot;[:punct:]&quot;)) %&gt;%
  mutate(tolower = str_squish(tolower)) %&gt;%
  inner_join({
    &quot;journal_abbreviations.txt&quot; %&gt;%
      read_lines() %&gt;%
      as_tibble() %&gt;%
      select(journal = value) %&gt;%
      separate(
        col = journal,
        into = c(&quot;journal&quot;, &quot;abbreviation&quot;),
        sep = &quot; = &quot;
      ) %&gt;%
      mutate(tolower = str_to_lower(journal)) %&gt;%
      mutate(tolower = str_remove_all(tolower, &quot;[:punct:]&quot;)) %&gt;%
      mutate(tolower = str_squish(tolower)) %&gt;%
      identity()
  }, by = &quot;tolower&quot;) %&gt;%
  filter(is.na(journalAbbreviation)) %&gt;%
  inner_join(zot_item_values, by = c(&quot;abbreviation&quot; = &quot;value&quot;)) %&gt;%
  identity() %$%
  walk2(valueID, itemID, update_sql)</code></pre>
<p>I hope that this was valuable, however, this is at least a way for me to keep track of how I did this.</p>
