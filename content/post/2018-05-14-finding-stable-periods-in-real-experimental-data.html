---
title: Finding 'Stable' Periods in Real Experimental Data
author: Hardi Koortzen
date: '2018-05-14'
slug: finding-stable-periods-in-real-experimental-data
categories:
  - R
tags:
  - R
  - rvest
  - stable periods
  - tidyverse
subtitle: ''
---



<p>In my daily life, I need to work with experimental data and evaluate these. These data comes from real experiments, and can contain periods where the experimental conditions were changed, or some upset occurred, and the reactor needed to be restarted. This results in data that should not be used during the model development or data analysis, as these are not stable periods. Now, before going into more details, what is meant by a ‘stable’ period. This is a period, time-on-stream, where no changes were made, thus allowing one to see the behaviour of a catalyst over time. These periods can allow one to determine the deactivation of the catalyst, and allows to use these in the kinetic modelling of the process, neither of which will be discussed here. Since this is difficult data to simulate, real world examples will be used, thus, the reader will not be able to replicate this process. The data was also <a href="https://en.wikipedia.org/wiki/Normalization_(statistics)">normalised</a>, utilising the <a href="https://en.wikipedia.org/wiki/Feature_scaling">feature scaline</a>. This was done to normalise the data, and also scale the data, such that all the variables are of comparable range. These steps are not shown here, as the data is proprietary. After transforming the data, it was saved to disk.</p>
<pre class="r"><code>library(&quot;tidyverse&quot;)

raw_data &lt;- read_rds(here::here(&quot;static/data/raw_data-ex.rds&quot;))</code></pre>
<pre><code>## Observations: 2,679
## Variables: 7
## $ hours_on_stream &lt;dbl&gt; 0.000000, 4.582001, 9.164639, 13.747277, 18.32...
## $ temperature     &lt;dbl&gt; 2.717832, 5.139889, 7.165234, 7.717604, 7.7531...
## $ pressure        &lt;dbl&gt; 51.57436, 51.71338, 51.70039, 51.54902, 51.293...
## $ co_in           &lt;dbl&gt; 55.03378, 55.16311, 55.15103, 55.01021, 54.772...
## $ h2_in           &lt;dbl&gt; 55.04746, 55.17675, 55.16467, 55.02389, 54.786...
## $ inert_in        &lt;dbl&gt; 22.59148, 22.61021, 22.60846, 22.58806, 22.553...
## $ fts_activity    &lt;dbl&gt; 19.62824, 18.21668, 21.02587, 23.07660, 22.615...</code></pre>
<p>Let’s first look at the raw data.</p>
<div class="figure" style="text-align: center"><span id="fig:plt-raw-normalised"></span>
<img src="/post/2018-05-14-finding-stable-periods-in-real-experimental-data_files/figure-html/plt-raw-normalised-1.png" alt="Normalised variables versus normalised hours on stream." width="95%" />
<p class="caption">
Figure 1: Normalised variables versus normalised hours on stream.
</p>
</div>
<p>This is a busy figure, however, it demonstrates the complexity of finding a ‘stable’ period. We cannot just look at one variable, i.e. temperature, and decide that since this did not change, this constitutes a stable period. We need to look at all the variables, and then find the regions where all these were kept constant. It is easy to assume we can just use <code>group_by()</code> to group all the variables together, however, there are small changes in these variables, that will increase the stable regions unnecessarily. We therefore need to define our own version of what these variable should look like, without all the small changes. This can be done by rounding. We cannot just round the variables to the nearest whole number, as the changes are too small, and this will result in a few regions, where stability was not obtained. This process is time consuming and relies on trail and error, and some knowledge of the underlying data variability. Another feature is the increase in hours. Although this might not seem to have an impact, however, there are some instances where the temperature was increased, and then later decreased, just to be increased again. Grouping by just the temperature will result in those regions being treated as similar, where in reality they are not. Look at the temperatures before 2000 h and then again after 3000 h. The <code>round()</code> function can round a number to the desired digits, however, with this data some fine tuning of this is required. The <code>round_any()</code> function for the <code>plyr</code> package is the function for this. This allows one to specify the rounding, thus ensuring better control of the rounding. Instead of just rounding to the nearest decimal, one can have a different number to round to, such as the nearest 1.5. Let’s see how this affects the data for the <code>temperature</code> variable.</p>
<pre class="r"><code>raw_data %&gt;%
  mutate(., stable_temperature = round_any(temperature, 1.5)) -&gt;
  stable_data</code></pre>
<div class="figure" style="text-align: center"><span id="fig:plt-zoomed"></span>
<img src="/post/2018-05-14-finding-stable-periods-in-real-experimental-data_files/figure-html/plt-zoomed-1.png" alt="Zoomed in section after the rounding of the temperature variable." width="95%" />
<p class="caption">
Figure 2: Zoomed in section after the rounding of the temperature variable.
</p>
</div>
<p>Now we have some areas where the temperature does remain stable. Rounding numbers will result in some differences, however, the majority of the regions can be considered ‘stable’. This process can now be repeated for the other variables. This will not be discussed in detail, just the final result will be shown. These <code>stable_*</code> variables are just temporary and will be discarded later. For the purpose of modelling or other analysis, the original values are required, as rounding will introduce some artificial artefacts.</p>
<pre class="r"><code>raw_data %&gt;%
  mutate(
    .,
    stable_hours = round_any(hours_on_stream, 500),
    stable_temperature = round_any(temperature, 1.5),
    stable_pressure = round_any(pressure, 5),
    stable_co_in = round_any(co_in, 5),
    stable_h2_in = round_any(h2_in, 5),
    stable_inert_in = round_any(inert_in, 2.5)
  ) -&gt;
  stable_data</code></pre>
<pre><code>## Observations: 2,679
## Variables: 13
## $ hours_on_stream    &lt;dbl&gt; 0.000000, 4.582001, 9.164639, 13.747277, 18...
## $ temperature        &lt;dbl&gt; 2.717832, 5.139889, 7.165234, 7.717604, 7.7...
## $ pressure           &lt;dbl&gt; 51.57436, 51.71338, 51.70039, 51.54902, 51....
## $ co_in              &lt;dbl&gt; 55.03378, 55.16311, 55.15103, 55.01021, 54....
## $ h2_in              &lt;dbl&gt; 55.04746, 55.17675, 55.16467, 55.02389, 54....
## $ inert_in           &lt;dbl&gt; 22.59148, 22.61021, 22.60846, 22.58806, 22....
## $ fts_activity       &lt;dbl&gt; 19.62824, 18.21668, 21.02587, 23.07660, 22....
## $ stable_hours       &lt;dbl&gt; 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0...
## $ stable_temperature &lt;dbl&gt; 3.0, 4.5, 7.5, 7.5, 7.5, 7.5, 7.5, 7.5, 7.5...
## $ stable_pressure    &lt;dbl&gt; 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50,...
## $ stable_co_in       &lt;dbl&gt; 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55,...
## $ stable_h2_in       &lt;dbl&gt; 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55,...
## $ stable_inert_in    &lt;dbl&gt; 22.5, 22.5, 22.5, 22.5, 22.5, 22.5, 22.5, 2...</code></pre>
<div class="figure" style="text-align: center"><span id="fig:plt-rounded-data"></span>
<img src="/post/2018-05-14-finding-stable-periods-in-real-experimental-data_files/figure-html/plt-rounded-data-1.png" alt="Rounded variables versus rounded hours." width="95%" />
<p class="caption">
Figure 3: Rounded variables versus rounded hours.
</p>
</div>
<p>Now at least, we can see some clear stable sections. The next bit gets a bit fuzzy, and took some time to implement. The aim here is to remove the data points that are outside the stable regions. First we need to group the data and then nest the variables we want to keep, this is essential for later, since these include the original data.</p>
<pre class="r"><code>stable_data %&lt;&gt;%
  group_by(
    .,
    stable_hours,
    stable_temperature,
    stable_pressure,
    stable_h2_in,
    stable_co_in,
    stable_inert_in
  ) %&gt;%
  nest(.)</code></pre>
<pre><code>## Observations: 181
## Variables: 7
## $ stable_hours       &lt;dbl&gt; 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5...
## $ stable_temperature &lt;dbl&gt; 3.0, 4.5, 7.5, 9.0, 12.0, 13.5, 15.0, 19.5,...
## $ stable_pressure    &lt;dbl&gt; 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50,...
## $ stable_h2_in       &lt;dbl&gt; 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55,...
## $ stable_co_in       &lt;dbl&gt; 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55,...
## $ stable_inert_in    &lt;dbl&gt; 22.5, 22.5, 22.5, 22.5, 22.5, 22.5, 22.5, 2...
## $ data               &lt;list&gt; [&lt;# A tibble: 1 x 7,   hours_on_stream tem...</code></pre>
<p>Now we have only the ‘stable’ periods, with all our original data nested in the <code>data</code> column. Next is to give these numbers, with a sequence (<code>seq_len()</code>) from 1 to 181. We will use these to remove the unwanted data. After unnesting, we have a column (<code>period</code>), containing numbers.</p>
<pre class="r"><code>stable_data %&lt;&gt;%
  mutate(., period = seq_len(nrow(.))) %&gt;%
  unnest(.)</code></pre>
<pre><code>## Observations: 2,679
## Variables: 14
## $ stable_hours       &lt;dbl&gt; 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0...
## $ stable_temperature &lt;dbl&gt; 3.0, 4.5, 7.5, 7.5, 7.5, 7.5, 7.5, 7.5, 7.5...
## $ stable_pressure    &lt;dbl&gt; 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50,...
## $ stable_h2_in       &lt;dbl&gt; 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55,...
## $ stable_co_in       &lt;dbl&gt; 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55,...
## $ stable_inert_in    &lt;dbl&gt; 22.5, 22.5, 22.5, 22.5, 22.5, 22.5, 22.5, 2...
## $ period             &lt;int&gt; 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3...
## $ hours_on_stream    &lt;dbl&gt; 0.000000, 4.582001, 9.164639, 13.747277, 18...
## $ temperature        &lt;dbl&gt; 2.717832, 5.139889, 7.165234, 7.717604, 7.7...
## $ pressure           &lt;dbl&gt; 51.57436, 51.71338, 51.70039, 51.54902, 51....
## $ co_in              &lt;dbl&gt; 55.03378, 55.16311, 55.15103, 55.01021, 54....
## $ h2_in              &lt;dbl&gt; 55.04746, 55.17675, 55.16467, 55.02389, 54....
## $ inert_in           &lt;dbl&gt; 22.59148, 22.61021, 22.60846, 22.58806, 22....
## $ fts_activity       &lt;dbl&gt; 19.62824, 18.21668, 21.02587, 23.07660, 22....</code></pre>
<p>Next we need to remove some of the values. The question is which ones? This depends on the period of your time data, and how many of these points you want in the stable region. In order to achieve this, we can use the <code>lead()</code> and <code>lag()</code> functions, from <code>dplyr</code>. This allows us do arithmetic on next (lead) and previous (lag) values. Next we subtract the <code>lag()</code> from the <code>lead()</code>, leading to a column (<code>stable_lead</code>) containing the result.</p>
<pre class="r"><code>stable_data %&lt;&gt;%
  mutate(., stable_lead = lead(period, n = 3) - lag(period, n = 3))</code></pre>
<pre><code>## Observations: 2,679
## Variables: 15
## $ stable_hours       &lt;dbl&gt; 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0...
## $ stable_temperature &lt;dbl&gt; 3.0, 4.5, 7.5, 7.5, 7.5, 7.5, 7.5, 7.5, 7.5...
## $ stable_pressure    &lt;dbl&gt; 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50,...
## $ stable_h2_in       &lt;dbl&gt; 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55,...
## $ stable_co_in       &lt;dbl&gt; 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55,...
## $ stable_inert_in    &lt;dbl&gt; 22.5, 22.5, 22.5, 22.5, 22.5, 22.5, 22.5, 2...
## $ period             &lt;int&gt; 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3...
## $ hours_on_stream    &lt;dbl&gt; 0.000000, 4.582001, 9.164639, 13.747277, 18...
## $ temperature        &lt;dbl&gt; 2.717832, 5.139889, 7.165234, 7.717604, 7.7...
## $ pressure           &lt;dbl&gt; 51.57436, 51.71338, 51.70039, 51.54902, 51....
## $ co_in              &lt;dbl&gt; 55.03378, 55.16311, 55.15103, 55.01021, 54....
## $ h2_in              &lt;dbl&gt; 55.04746, 55.17675, 55.16467, 55.02389, 54....
## $ inert_in           &lt;dbl&gt; 22.59148, 22.61021, 22.60846, 22.58806, 22....
## $ fts_activity       &lt;dbl&gt; 19.62824, 18.21668, 21.02587, 23.07660, 22....
## $ stable_lead        &lt;int&gt; NA, NA, NA, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0...</code></pre>
<p>As seen in <code>stable_lead</code>, there are some <code>NA</code>‘s, due to the <code>lead()</code> and <code>lag()</code> output. We are interested in the rows where <code>stable_lead == 0</code>, this indicates that the calculated rows does not differ from the previous points. Values other than zero, indicates that there are difference, thus these are not in the ’stable’ region of the calculated values. We can remove the unwanted values with the <code>filter()</code> function.</p>
<pre class="r"><code>stable_data %&lt;&gt;%
  filter(., stable_lead == 0) %&gt;%
  ungroup(.)</code></pre>
<pre><code>## Observations: 1,960
## Variables: 15
## $ stable_hours       &lt;dbl&gt; 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0...
## $ stable_temperature &lt;dbl&gt; 7.5, 7.5, 7.5, 7.5, 7.5, 7.5, 7.5, 7.5, 7.5...
## $ stable_pressure    &lt;dbl&gt; 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50,...
## $ stable_h2_in       &lt;dbl&gt; 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55,...
## $ stable_co_in       &lt;dbl&gt; 55, 55, 55, 55, 55, 55, 55, 55, 55, 55, 55,...
## $ stable_inert_in    &lt;dbl&gt; 22.5, 22.5, 22.5, 22.5, 22.5, 22.5, 22.5, 2...
## $ period             &lt;int&gt; 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3...
## $ hours_on_stream    &lt;dbl&gt; 22.91192, 27.49455, 41.24183, 45.82383, 50....
## $ temperature        &lt;dbl&gt; 7.686912, 7.825015, 7.855704, 7.671570, 7.6...
## $ pressure           &lt;dbl&gt; 51.18783, 50.89095, 50.56938, 50.56679, 50....
## $ co_in              &lt;dbl&gt; 54.67418, 54.39797, 54.09880, 54.09639, 54....
## $ h2_in              &lt;dbl&gt; 54.68796, 54.41184, 54.11276, 54.11035, 54....
## $ inert_in           &lt;dbl&gt; 22.53940, 22.49941, 22.45609, 22.45574, 22....
## $ fts_activity       &lt;dbl&gt; 25.68224, 24.70261, 29.65833, 31.64993, 31....
## $ stable_lead        &lt;int&gt; 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0...</code></pre>
<p>Next we can remove all the <code>stable_*</code> columns, as these were just to get us to this point. The <code>select()</code> functions helps here.</p>
<pre class="r"><code>stable_data %&lt;&gt;%
  select(., -starts_with(&quot;stable_&quot;)) %T&gt;%
  write_rds(., here::here(&quot;static/data/stable_data-ex.rds&quot;), &quot;xz&quot;)</code></pre>
<pre><code>## Observations: 1,960
## Variables: 8
## $ period          &lt;int&gt; 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 9...
## $ hours_on_stream &lt;dbl&gt; 22.91192, 27.49455, 41.24183, 45.82383, 50.406...
## $ temperature     &lt;dbl&gt; 7.686912, 7.825015, 7.855704, 7.671570, 7.6715...
## $ pressure        &lt;dbl&gt; 51.18783, 50.89095, 50.56938, 50.56679, 50.564...
## $ co_in           &lt;dbl&gt; 54.67418, 54.39797, 54.09880, 54.09639, 54.094...
## $ h2_in           &lt;dbl&gt; 54.68796, 54.41184, 54.11276, 54.11035, 54.108...
## $ inert_in        &lt;dbl&gt; 22.53940, 22.49941, 22.45609, 22.45574, 22.455...
## $ fts_activity    &lt;dbl&gt; 25.68224, 24.70261, 29.65833, 31.64993, 31.696...</code></pre>
<p>Now, a look at the ‘stable’ data.</p>
<div class="figure" style="text-align: center"><span id="fig:plt-stable-data"></span>
<img src="/post/2018-05-14-finding-stable-periods-in-real-experimental-data_files/figure-html/plt-stable-data-1.png" alt="The stable period data." width="95%" />
<p class="caption">
Figure 4: The stable period data.
</p>
</div>
<p>The changes in this graph is subtle, and one needs to squint to see difference, and that is the point here. We do not want to remove all the data, just sections where instability occurred. This could be due to a change in the variables or an upset in the reactor, resulting in a restart, either way, these transient behaviour is not of interest to us, as they are short and the reactor will ‘settle’ down, resulting in a stable operation. This process can be repeated, especially on a particular long experiment, to fine tune the rounding values, or even the <code>lead()</code> and <code>lag()</code> arithmetic, to either include more or less points in the ‘stable’ periods.</p>
<p>While writing this, it occurred to me, that it would be good to put all this in a function. Now, before you start laughing here, I did put this into a function, however, I hard coded the columns, the rounding, etc. What I meant, was it would be good to make this a bit less specific, and include it as a general function, since this is not the only experimental data I am working with. In the other cases, some of the variables are not used, or new ones exists, requiring several functions for each process. A more ‘generic’ function could be used to achieve this. This is probably for later, as I need to first learn how to do this, and this might take awhile to realise.</p>
<p>Since we have these stable periods, it would be good to get averages of these. During the model development, these average stable points can then be used to obtain the model description. This was a place I was stuck on for a bit. I think I wanted to overcomplicate the call, and I just could not figure out how to get the averages of these stable periods. Since we did all the work in finding the stable periods, and creating a column named <code>period</code>, we can just use this to group our data, and then determine the averages of the required variables.</p>
<pre class="r"><code>stable_data %&gt;%
  group_by(., period) %&gt;%
  summarise_all(., mean) %&gt;%
  ungroup(.) %T&gt;%
  write_rds(., here::here(&quot;static/data/mean_data-ex.rds&quot;), &quot;xz&quot;) -&gt;
  mean_data</code></pre>
<pre><code>## Observations: 94
## Variables: 8
## $ period          &lt;int&gt; 3, 9, 15, 18, 20, 21, 22, 23, 24, 26, 27, 28, ...
## $ hours_on_stream &lt;dbl&gt; 64.76502, 192.46252, 393.59353, 598.77744, 710...
## $ temperature     &lt;dbl&gt; 7.627123, 18.695890, 29.779518, 35.304643, 37....
## $ pressure        &lt;dbl&gt; 51.10702, 51.93409, 51.95614, 52.11810, 51.661...
## $ co_in           &lt;dbl&gt; 54.59899, 55.36845, 55.38897, 55.53964, 55.115...
## $ h2_in           &lt;dbl&gt; 54.61279, 55.38202, 55.40253, 55.55316, 55.128...
## $ inert_in        &lt;dbl&gt; 22.52852, 22.63994, 22.64291, 22.66473, 22.603...
## $ fts_activity    &lt;dbl&gt; 32.55112, 36.37621, 37.21062, 35.46528, 35.010...</code></pre>
<div class="figure" style="text-align: center"><span id="fig:plt-mean-data"></span>
<img src="/post/2018-05-14-finding-stable-periods-in-real-experimental-data_files/figure-html/plt-mean-data-1.png" alt="The average stable period data." width="95%" />
<p class="caption">
Figure 5: The average stable period data.
</p>
</div>
<p>And now we have data for modelling and utilising further, without the worry about data that are not stable. That is a discussion for another time.</p>
